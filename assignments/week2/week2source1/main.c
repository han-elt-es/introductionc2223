/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week2source1
 *  /author John van den Hooven
 *  /organisation HAN
*/

#include <stdio.h>

int main(void)
{
   printf("Hello World C programming!\n");

   /* remove "//" in the following line when starting exercise) */
   // printf(....);

   /* remove "//" in the following line when starting exercise * 5) */
   // printf("Integer value = %d\nfloating point value = % lf\n ", ....);

   return 0;
}
