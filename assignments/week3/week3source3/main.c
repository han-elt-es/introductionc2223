/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week2source3
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>

int main(void)
{
   int code;
   code = 2;
   switch(code)
   {
      case 1:
         printf("Your code is 1\n");
         break;
      case 2:
         printf("Your code is 2\n");
      case 3:
         printf("Your code is 3\n");
         break;
      default:
         printf("Silence is golden\n");
         break;
   }
   return 0;
}
