/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week2source6
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>

int main()
{
   enum pizzaselect {QUATROSTAGIONI, NAPOLITANA, MARGHERITA};

   enum pizzaselect choice;
   printf("Please select one of the following options:\n");
   printf("%d)Pizza Quatro Stagioni\n", QUATROSTAGIONI);
   printf("%d)Pizza Napolitana\n", NAPOLITANA);
   printf("%d)Pizza Margherita\n", MARGHERITA);
   printf("\n Enter your choice a, b or c: ");
   choice = getchar();

   switch (choice) {
      case QUATROSTAGIONI:
         printf("You selected Pizza Quatro Stagioni\n");
         break;
      case NAPOLITANA:
         printf("You selected Pizza Napolitana\n");
         break;
      case MARGHERITA:
         printf("You selected Pizza Margherita\n");
         break;
      default:
         printf("\n Choice not available, options are a, b or c: \n");
         break;
   }


   return 0;
}
