/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week2source5
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>

int main(void)
{
   int i;
   int x;
   for (i = 0; i < 5; i++)
   {
      printf("The value of i is now %d\n", i);
   }

   x = 0;
   while(x < 4)
   {
      x = x + 1;
      printf("The value of x is now %d\n", x);
   }
   return 0;
}
