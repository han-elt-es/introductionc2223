/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week3source1
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>

#define ANUMBER 8

int main(void)
{
   int a;
   int b;
   int c;

   a = 6;
   b = 4;
   c = 7;

   if( a == b)
   {
      printf("1) %d is equal to %d\n", a, b);
   }
   else
   {
      printf("1) %d is not equal to %d\n", a, b);
   }

   if( a != ANUMBER)
   {
      printf("2) %d is not equal to a number: %d\n", a, ANUMBER);
   }
   else
   {
      printf("2) %d is equal to a number: %d\n", a, ANUMBER);
   }

   if( b == ANUMBER)
   {
      printf("3) %d is equal to a number: %d\n", b, ANUMBER);
   }
   else
   {
      printf("3) %d is not equal to a number: %d\n", b, ANUMBER);
   }

   if( b = ANUMBER)
   {
      printf("4) %d is equal to a number: %d\n", b, ANUMBER);
   }
   else
   {
      printf("4) %d is not equal to a number: %d\n", b, ANUMBER);
   }
   return 0;
}
