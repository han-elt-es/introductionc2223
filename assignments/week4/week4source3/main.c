/* Random number generation example
 * example adapted from: (c) C programming for dummies, Dan Gooking
 * Listing 11-10
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week4source3
 *  /author John van den Hooven
 *  /organisation HAN
*/

void initRandom(void);
int generateSecret(int max);

int main(void)
{
   int r;
   srand((unsigned)time(NULL));
   for(int i = 0; i < 20; i++)
   {
      for (int j = 0; j < 5; j++)
      {
         r = rand();
         printf("%d\t", r);
      }
      putchar('\n');
   }
   printf("result is limited to 100\n");

   for(int i = 0; i < 20; i++)
   {
      for (int j = 0; j < 5; j++)
      {
         r = rand() % 100;
         printf("%d\t", r);
      }
      putchar('\n');
   }

   return 0;
}
