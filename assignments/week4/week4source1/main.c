#include <stdio.h>

/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week4source1
 *  /author John van den Hooven
 *  /organisation HAN
*/

double CalcVoltage(double I, double R);

int main(void)
{
   double V = 0.0;
   double I = 10.0;
   double R = 20000.0;

   printf("Ohms calculator:\n");


   /* Calculate V, replace this with the function */
   V = CalcVoltage(I, R);

   /* Print all values: V, I and R */
   printf("V = %.1lf V\n", V);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   /* Increase the resistance R by 5000 Ohm,
    * I remains the same value */

   //R = .... + 5000;

   /* Recalculate V, use the formula */

   // use scanf() to get a value for U and for I and calculate R

   return 0;
}

/* Functions implementation */

double CalcVoltage(double I, double R)
{
   // Complete the function implementation
   // the calculated voltage will be returned
   return 0;
}

