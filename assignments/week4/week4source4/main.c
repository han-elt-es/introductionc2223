/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week2source5
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>
#include "secret.h"

int main(void)
{
   int secretNumber;
   initRandom();

   secretNumber = generateSecret(100);
   printf("Secretnumber is: %d\n", secretNumber);
   secretNumber = generateSecret(999);
   printf("Secretnumber is: %d\n", secretNumber);
   secretNumber = generateSecret(2);
   printf("Secretnumber is: %d\n", secretNumber);
   secretNumber = generateSecret(1);
   printf("Secretnumber is: %d\n", secretNumber);
}
