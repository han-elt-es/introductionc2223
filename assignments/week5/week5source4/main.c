/*  HAN University of Applied Sciences - Academy Engineering and Automotive
 *  Introduction C Programming - week4source2
 *  /author John van den Hooven
 *  /organisation HAN
*/
#include <stdio.h>

double calculator(double a, double b, char action);
void calculatorWithReference(double * result, double a, double b, char action);

int main(void)
{
   double a = 0;
   double b = 0;
   double result = 0;
   char action;

   printf("Simple calculator\n");
   printf("Please enter the first value: ");
   scanf("%lf", &a);
   fflush(stdin);
   printf("Please enter the action ( +, -, *, or /: )");
   action = getchar();
   printf("Please enter the second value: ");
   scanf("%lf", &b);
   fflush(stdin);
   result = calculator(a, b, action);
   printf("%lf %c %lf = %lf\n", a, action, b, result);
   return 0;
}

double calculator(double a, double b, char action)
{
   double result;
   switch (action) {
      case '+':
         result = a + b;
         break;
      case '-':
         result = a - b;
         break;
      case '*':
         result = a * b;
         break;
      case '/':
         result = a / b;
         break;
      default:
         return -1;
         break;
   }
   return result;
}

void calculatorWithReference(double * result, double a, double b, char action)
{
   switch (action) {
      case '+':
      case '-':
      case '*':
      case '/':
      default:
         break;
   }
}
