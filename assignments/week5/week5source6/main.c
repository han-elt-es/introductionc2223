#include <stdio.h>
#include <stdlib.h>

#define NUMBEROFINTEGERS 10

int main(void)
{
   char ch;
   char lines[50][80];
   /// File handle fh
   FILE *fh;
   /// open the file
   fh = fopen("hello.txt", "w");

   fprintf(fh, "1. Some text to fill this file\n");
   fprintf(fh, "2. Some text to fill this file\n");
   fprintf(fh, "3. Some text to fill this file\n");
   fprintf(fh, "4. Some text to fill this file\n");
   fprintf(fh, "5. Some text to fill this file\n");
   fprintf(fh, "6. Some text to fill this file\n");
   fclose(fh);
   printf("File Written\n");
   fh = fopen("hello.txt", "r");
   do
   {
      ch=fgetc(fh);
      if (ch==EOF )
      {
         putchar(ch);
         break;
      }
      putchar(ch);
   }
   while (!feof(fh));
   fclose(fh);
   printf("File Read\n");
   return 0;
}
