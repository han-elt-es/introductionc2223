#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   /// File handle fh
   FILE *fh;
   /// open the file
   fh = fopen("hello.txt", "w");

   fprintf(fh, "Some text to fill this file\n");
   fclose(fh);
   printf("File Written\n");
   return 0;
}
