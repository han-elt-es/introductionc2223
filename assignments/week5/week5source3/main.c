#include <stdio.h>

#define ARRSIZE 5

void function2(int arr);
void printArray(int *arr, int size);
void fillArray(int *arr, int size);

int main(void)
{
   // Array definition
   int intArray[ARRSIZE];

   // Fill this array
   fillArray(intArray, ARRSIZE);

   printf("\nBefore function2 call:\n");
   printf("value of intArray[3]: %d\n", intArray[3]);

   function2(&intArray[3]);

   printf("After function2 call:\n");
   printf("value of intArray[3]: %d\n", intArray[3]);

   return 0;
}

void function2(int arr)
{
   printf("Function call (arr = %d)\n", arr);
   arr = 33;
   printf("\n");

}

void printArray(int *arr, int size)
{
   for (int i = 0; i < size; i++)
   {
      printf("%d\t", arr[i]);
   }
   printf("\n");
}


void fillArray(int *arr, int size)
{
   for (int i = 0; i < size; i++)
   {
      arr[i] = i;
   }
}
