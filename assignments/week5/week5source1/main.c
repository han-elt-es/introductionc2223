
#include <stdio.h>

int function1(int a, int b);
int function2(int *a, int b);
void function3(int *a, int b);

int main(void)
{
   int result;
   result = function1(10, 20);
   printf("Function1 result = %d\n", result);
   result = 10;
   result = function2(&result, 20);
   printf("Function2 result = %d\n", result);
   result = 10;
//   function3(....);
   printf("Function3 result = %d\n", result);

   return 0;
}

int function1(int a, int b)
{
   printf("function1: Value of a in the function: %d\n", a);
   printf("function1: Value of b in the function: %d\n", b);
   return a + b;
}

int function2(int *a, int b)
{
   printf("function1: Value of a in the function: %d\n", a);
   printf("function1: Value of b in the function: %d\n", b);
   return a + b;
}

void function3(int *a, int b)
{
   printf("function1: Value of a in the function: %d\n", a);
   printf("function1: Value of b in the function: %d\n", b);
   a = *a + b;
}
