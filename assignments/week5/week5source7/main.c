#include <stdio.h>
#define FILENAME "score.txt"

int main()
{
   FILE *fh;
   char name[25];
   int guesses;
   int guess;
   char hint[7];


   // Open a file and fill the file with (dummy) information
   fh = fopen(FILENAME,"a");
   if (fh == NULL)
   {
      puts("Problem opening the file (write)");
      return -1;
   }
   fprintf(fh, "john\t4\t45\tlow\t22\tlow\t64\tlow\t99\thigh\n");
   fprintf(fh, "henk\t4\t45\tlow\t22\tlow\t64\tlow\t65\tsucces\n");
   fprintf(fh, "john\t3\t45\tlow\t22\tlow\t65\tsucces\n");
   fclose(fh);
   // read (and print) scores
   fh = fopen(FILENAME,"r");
   if (fh == NULL)
   {
      puts("Problem opening the file (read)");
      return -1;
   }
   while (!feof(fh))
   {
      fscanf(fh, "%s", name);
      if (feof(fh))
      {
         break;
      }
      printf("%s\t", name);
      fscanf(fh, "%d", &guesses);
      for (int i = 0; i < guesses && !feof(fh); i++)
      {
         fscanf(fh, "%d", &guess);
         fscanf(fh, "%s", hint);
         printf("%d\t%s\t", guess, hint);
      }
      puts("\n");
   }
   fclose(fh);
   return 0;
}
