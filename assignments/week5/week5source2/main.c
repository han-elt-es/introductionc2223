#include <stdio.h>

#define ARRSIZE 5

void function1(int *arr, int size);
void printArray(int *arr, int size);

int main(void)
{
   // Array definition
   int intArray[ARRSIZE];
   // Fill this array
   for (int i = 0; i < ARRSIZE; i++)
   {
      intArray[i] = i;
   }

   printf("Before function1 call:\n");
   printArray(intArray, ARRSIZE);

   function1(intArray, ARRSIZE);

   printf("After function1 call:\n");
   printArray(intArray, ARRSIZE);
   return 0;
}

void function1(int *arr, int size)
{
   printf("Function call (arr = %d)\n", arr);
   printArray(arr, size);
   printf("\n");
   for (int i = 0; i < size; i++)
   {
      arr[i] = 44;
   }
}


void printArray(int *arr, int size)
{
   for (int i = 0; i < size; i++)
   {
      printf("%d\t", arr[i]);
   }
   printf("\n");
}
