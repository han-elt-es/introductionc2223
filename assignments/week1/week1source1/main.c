#include <stdio.h>
/* Introduction C programming
 * Assignment Code
 * Assignment Week 1 Source 1
 * (c) HAN, John van den Hooven
 *
 */
int main()
{
printf("Why is programming fun?\n");
printf("What delights may its practitioner expect as his reward?\n");
       printf("First is the sheer joy of making things.\n");
              printf("As the child delights in his mud pie,\n");
                     printf(" so the adult enjoys building things,\n");
                            printf(" especially things of his own design.\n");
                     printf("I think this delight must\n");
                     printf(" be an image of God's delight in making things,\n");
                            printf(" a delight shown in the distinctness and newness\n");
                                  printf(" of each leaf and each snowflake.\n");
printf("-- Frederick P. Brooks, Jr. from MythicalManMonth\n\n");

}
