#include <stdio.h>

int main(void)
{
   char letter;
   char str1[20];

   printf("Enter one character: ");
   scanf("%c", &letter);

   printf("Enter your first name: ");
   scanf("%s", str1);

   printf("Hi %s ", str1);
   printf("you have entered the letter: %c\n", letter);

   return (0);
}
