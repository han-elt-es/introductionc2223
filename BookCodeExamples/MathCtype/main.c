#include <ctype.h>
#include <math.h>
#include <stdio.h>

#define PI 3.1415

// This program demonstrates the use of two standard C libraries
// - ctype: gives us functions to determinate the type of a char
// - math: delivers a lot of mathematical functions
int main(void)
{
   char letter1 = 'a';
   char letter2 = '.';
   double x = 0.5 * PI;

   // show some information about letters (ctype.h)
   printf("The case of letter1 is: %s\n",
          isupper(letter1) ? "upper" : "lower");

   printf("The letter2 is: %s a punctuation character\n\n",
          ispunct(letter2) ? "" : "not");

   // Do some math (math.h)
   printf("The cosine of x (radians) is: %lf\n", cos(x));
   printf("The square root of x is: %lf\n", sqrt(x));

   return 0;
}
