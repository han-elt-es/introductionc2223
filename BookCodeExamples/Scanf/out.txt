Enter one integer: 1
Enter one floating point double: 2.3
Enter one character: a
Enter string (max. 9 characters): abcdefg
String: abcdefg
Enter string using spaces and tabs (max. 9 characters): abc sed ss
String: abc sed ss

1 2.300000   abc sed    ss

Length of the string in 'string'' is: 10

Enter two integers, value between 1 and 100: 2
 1
Result:  2 +  1 =   3
Enter + or - sign: -
Result is ok: -
