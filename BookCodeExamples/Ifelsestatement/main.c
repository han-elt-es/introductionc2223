#include <stdio.h>

/* Using an if statement */
int main(void)
{
   int a = 5;

   /* if statement with an alternative path */
   if (a > 6)
   {
      printf("a (%d) is greater than 6\n", a);
   }
   else
   {
      printf("a (%d) is not greater than 6\n", a);
   }

   /* change the value of a */
   a = 7;
   if (a > 6)
   {
      printf("a (%d) is greater than 6\n", a);
   }
   else
   {
      printf("a (%d) is not greater than 6\n", a);
   }

   a = 7;
   if (a > 7)
   {
      printf("a (%d) is greater than 7\n", a);
   }
   else
   {
      printf("a (%d) is not greater than 7\n", a);
   }

   if (a > 7)
   {
      printf("a (%d) is greater than 7\n", a);
   }
   else if (a == 7)
   {
      printf("a (%d) is 7\n", a);
   }
   else
   {
      printf("a (%d) is not 7\n", a);
   }

   return 0;
}
