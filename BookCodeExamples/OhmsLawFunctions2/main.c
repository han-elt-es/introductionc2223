/* Ohm's law calculations in functions */

#include <stdio.h>

/* Prototypes of the calculation functions used in this program */
double calcVoltage(double I, double R);
double calcCurrent(double U, double R);
double calcResistance(double U, double I);

/* Prototypes of the print functions used in this program */
void printAll(double I, const char Iname[], double R, const char Rname[],
              double U, const char Uname[]);
void printAllSingleLine(double I, const char Iname[], double R,
                        const char Rname[], double U, const char Uname[]);

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U1 = 0.0;
   double I1 = 5.2;
   double R1 = 32.0;

   /* Some function calls with different parameters */
   U1 = calcVoltage(I1, R1);
   printAll(I1, "I1", R1, "R1", U1, "U1");

   double U2 = 24.0;
   double R2 = 5.0;
   double I2 = calcCurrent(U2, R2);
   printAllSingleLine(I2, "I2", R2, "R2", U2, "U2");

   U2 = 5.0;
   I2 = 0.001;
   R2 = calcResistance(U2, I2);
   printAll(I2, "I2", R2, "R2", U2, "U2");

   return 0;
}

/* Complete declaration and definition of the calculation functions */
double calcVoltage(double I, double R)
{
   return I * R;
}

double calcCurrent(double U, double R)
{
   return U / R;
}

double calcResistance(double U, double I)
{
   return U / I;
}

/* Definitions using the print() functions */
void printAll(double I, const char Iname[], double R, const char Rname[],
              double U, const char Uname[])
{
   printf("%s = %.1lf A\n", Iname, I);
   printf("%s = %.1lf Ohm\n", Rname, R);
   printf("%s = %.1lf V\n\n", Uname, U);
}

void printAllSingleLine(double I, const char Iname[], double R,
                        const char Rname[], double U, const char Uname[])
{
   printf("%s = %.1lf A, %s = %.1lf Ohm, %s = %.1lf V\n\n", Iname, I,
          Rname, R, Uname, U);
}
