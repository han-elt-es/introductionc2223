#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bytebitfunc.h"

// val2str() we use a very short if else notation:
// (logical expression) ? <result if true> : <result if false>
// if (logical expression)
//    {
//      byte[i] = '1'
//    }
// else
//    {
//      byte[i] = '1'
//    }
// Find out yourself how this can be used (keyword: "ternary operator" in c).

void val2str(char byte[], unsigned char val, int lengthByte)
{
   int highbit;
   highbit = (int) pow(2, lengthByte-2);  // set the highest bit as mask
   for (int i = 0; i < lengthByte-1; i++)
   {
      byte[i] = (val & (highbit >> i))? '1' : '0';
   }
}

void printByte(unsigned char val, char text[])
{
   char out[9] = "";
   val2str(out, val, 9);
   printf("%25s %s\n", text, out);
}
