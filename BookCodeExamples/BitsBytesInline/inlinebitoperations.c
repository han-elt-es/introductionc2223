#include "inlinebitoperations.h"

// in stead of using macro's this is a set of inline bitwise operations

unsigned char setBit(unsigned char var, int bit)
{
   return var |= (1 << bit);
}

unsigned char clearBit(unsigned char var, int bit)
{
   return var &= ~(1 << bit);
}

unsigned char toggleBit(unsigned char var, int bit)
{
   return var ^= (1 << bit);
}

unsigned char testBitSet(unsigned char var, int bit)
{
   return var & (1 << bit);
}

unsigned char testBitclear(unsigned char var, int bit)
{
   return !(var & (1 << bit));
}

unsigned char setMask(unsigned char var, unsigned char mask)
{
   return (var | mask);
}

unsigned char clearMask(unsigned char var, unsigned char mask)
{
   return (var & ~mask);
}

