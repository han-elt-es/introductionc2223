TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        bytebitfunc.c \
        inlinebitoperations.c \
        main.c

HEADERS += \
   bytebitfunc.h \
   inlinebitoperations.h
