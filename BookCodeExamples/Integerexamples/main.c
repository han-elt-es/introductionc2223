#include <limits.h>
#include <stdio.h>

/* Integer definition examples */
int main(void)
{
   int a = 0;              // Integer initialized with 0
   unsigned int b = 32000; // unsigned integer initialized with 32000

   char ch1 = 'A';         // A character integer with the character A
                           // Character literals are enclosed with ' and '
   char ch2 = 65;          // A character integer with the character A
                           // Have a look in the ASCII table to confirm this

   /* Limits */
   printf("Maximum value int:           %d\n", INT_MAX);
   printf("Minimum value int:          %d\n", INT_MIN);
   printf("Maximum value unsigned int:  %u\n\n", UINT_MAX);

   /* Display the values of the variables */
   printf("Value of a: %d\n", a);
   printf("Value of b: %u\n\n", b);

   printf("Value of ch1 as character: %c\n", ch1);
   printf("Value of ch1 as integer: %d\n", ch1);
   printf("Value of ch1 + 1 as character: %c\n", ch1 + 1);
   printf("Value of ch1 + 2 as character: %c\n", ch1 + 2);

   printf("Value of ch2 as character: %c\n", ch2);
   printf("Value of ch2 as integer: %d\n\n", ch2);

   /* Some integer calculations */
   a = 25 * 3;
   printf("Integer result of integer multiplication 25 * 3: %d\n", a);
   a = 24 / 9;
   printf("Integer result of integer division       24 / 9: %d\n", a);

   return 0;
}
