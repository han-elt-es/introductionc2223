#include <stdio.h>
#include <string.h>

int main(void)
{
   char text[] = "Hello";

   /* Print text using printf */
   printf("Text: %s\n", text);

   /* Print text using putchar */
   putchar('T');
   putchar('e');
   putchar('x');
   putchar('t');
   putchar(':');
   putchar(' ');

   for (int i = 0; i < strlen(text); i++)
   {
      putchar(text[i]);
   }
   putchar('\n');

   return 0;
}
