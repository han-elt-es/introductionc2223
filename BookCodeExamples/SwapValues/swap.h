#ifndef SWAP_H
#define SWAP_H

void swapval(int a, int b);
void swapref(int *pa, int *pb);

#endif
