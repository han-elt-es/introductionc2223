TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        swap.c

HEADERS += \
   swap.h

DISTFILES += \
   out.txt
