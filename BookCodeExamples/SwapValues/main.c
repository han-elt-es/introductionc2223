#include "swap.h"

#include <stdio.h>

int main(void)
{
   int x = 10;
   int y = 20;

   printf("Swapping x and y by using function swapval:\n");

   printf("Before calling swapval values of x: %d\n", x);
   printf("Before calling swapval values of y: %d\n\n", y);

   swapval(x, y);

   printf("After calling swapval values of x: %d\n", x);
   printf("After calling swapval values of y: %d\n\n", y);

   printf("Swapping x and y by using function swapref:\n");

   printf("Before calling swapref values of x: %d\n", x);
   printf("Before calling swapref values of y: %d\n\n", y);

   swapref(&x, &y);

   printf("After calling swapref values of x: %d\n", x);
   printf("After calling swapref values of y: %d\n", y);

   return 0;
}
