#include <stdio.h>

#define SIZE 6

int main(void)
{
   int data[SIZE] = {-20, 3, -10, 5, -40, 0};

   printf("All negative values in data: ");
   for (int i = 0; i < SIZE; i++)
   {
      // Test for positive or zero value
      if (data[i] >= 0)
      {
         continue;
      }
      printf("%d ", data[i]);
   }
   printf("\n");

   return 0;
}
