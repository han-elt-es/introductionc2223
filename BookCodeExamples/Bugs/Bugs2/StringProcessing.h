#ifndef STRINGPROCESSING_H
#define STRINGPROCESSING_H

/*!
 * Counts the number of c characters in the input string text.
 */
int countChar(const char text[], char c);

/*!
 * Reverses the contents of the input string text.
 * Example: "123" becomes "321"
 */
void reverseString(char text[]);

#endif
