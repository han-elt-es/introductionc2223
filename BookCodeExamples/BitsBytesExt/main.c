#include <stdio.h>
#include "bytebitfunc.h"

// define a "byte" type, in fact it's an unsigend char
// but we want to use it as an byte of 8 bits

typedef unsigned char byte;

#define POWER 7         // led position 7
#define STARTED 6       // led position 6

//                00    01    02     03
enum errorcodes {NONE, TEMP, SPEED, BATTERY};

//                 00      01      02     03
enum speedcodes {SPEED4, SPEED3, SPEED2, SPEED1};

#define RESETSPEED 0b11110000
#define RESETERROR 0b11001111
#define RESETALL 0b00000000

int main(void)
{
   // use the byte type to control an array of 8 leds
   // from right to left:
   // power (1 bit)     =  bit 7
   // started (1 bit)   =  bit 6
   // error (2 bits)    =  bits 4 and 5
   // speed (4 bits)    =  bits 0, 1, 2, 3
   // "1" is on, "0" is off

   // define the statusDisplay control byte
   byte statusDisplay = RESETALL;
   enum errorcodes error = NONE;

   printf("%25s %s\n", "Status leds:", "PSEESSSS");

   // use an bitwise "or" to set power bit to on ("1")
   // the use of 0bxxxxxxxx is not C11 defined, it is a GNU compiler
   // extension.
   statusDisplay = statusDisplay | 0b10000000;
   printByte(statusDisplay, "Power on: ");

   // use an a bitwise "shift left" and a bitwise "or"
   // to set the started bit (7) on
   statusDisplay |= (1 << STARTED);
   printByte(statusDisplay, "Started on: ");

   // use an bitwise operation "or" to set SPEED1
   statusDisplay |= (1 << SPEED1);
   printByte(statusDisplay, "Speed is set to SPEED1: ");

   // use an bitwise operation "or" to set SPEED2
   statusDisplay |= (1 << SPEED2);
   printByte(statusDisplay, "Speed is set to SPEED2: ");
   // use an bitwise operation "or" to set SPEED2
   statusDisplay |= (1 << SPEED3);
   printByte(statusDisplay, "Speed is set to SPEED2: ");
   // use an bitwise operation "or" to set SPEED2
   statusDisplay |= (1 << SPEED4);
   printByte(statusDisplay, "Speed is set to SPEED2: ");

   // use an bitwise operation "and" to reset SPEED4
   statusDisplay &= ~(1 << SPEED4);
   printByte(statusDisplay, "Speed is set to SPEED3: ");
   // use an bitwise operation "and" to reset SPEED3
   statusDisplay &= ~(1 << SPEED3);
   printByte(statusDisplay, "Speed is set to SPEED2: ");
   // use an bitwise operation "and" to reset SPEED2
   statusDisplay &= ~(1 << SPEED2);
   printByte(statusDisplay, "Speed is set to SPEED1: ");


   // use an bitwise operation "and" to completly reset speed
   statusDisplay &= RESETSPEED;
   printByte(statusDisplay, "Speed is set to NONE: ");

   // use an a bitwise "shift left" and a bitwise "and"
   // to unset the started bit (7) on
   statusDisplay &= ~(1 << STARTED);
   printByte(statusDisplay, "Started off: ");

   // use an a bitwise "or" to set a TEMP error
   error = TEMP;
   statusDisplay |= (error << 4); // push the error to the right position
   printByte(statusDisplay, "Temperature error: ");

   // Reset the error
   statusDisplay &= (~error << 4);
   printByte(statusDisplay, "No error: ");

   return 0;
}


