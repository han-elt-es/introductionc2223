#include <stdio.h>
#include <string.h>
#include <time.h>

#define MAX_SIZE 30

int main(void)
{
   time_t time_start;
   time_t time_now;
   struct tm *pTime;
   char time_str[MAX_SIZE];

   /* Get current time in seconds */
   time(&time_start);
   /* Custom build the format of the date/time */
   pTime = localtime(&time_start);
   strftime(time_str, MAX_SIZE, "%D  %H:%M:%S", pTime);
   /* Display new format string */
   printf("Local time: %s\n-- Press a key ... ", time_str);

   /* Introduce some delay, wait for a keypress */
   getchar();

   /* Get current time in seconds */
   time(&time_now);
   pTime = localtime(&time_now);
   strftime(time_str, MAX_SIZE, "%D  %H:%M:%S", pTime);

   /* Display time and elapsed time in seconds */
   printf("\nLocal time: %s\n", time_str);

   printf("-- Elapsed time = %d sec.\n",
          (int)difftime(time_now, time_start));

   return 0;
}
