#include <stdio.h>

/* Nested loops */

int main(void)
{
   int a[3] = {1, 2, 3};
   int b[3] = {2, 3, 6};
   int result[9] = {0};
   int index = 0;

   for (int i = 0; i < 3; i++)
   {
      for (int j = 0; j < 3; j++)
      {
         result[index] = a[i] * b[j];
         index = index + 1;
      }
   }

   for (int i = 0; i < 9; i++)
   {
      printf("%d\n", result[i]);
   }

   return 0;
}
