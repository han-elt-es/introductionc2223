#include <stdio.h>

#define PI 3.14

int main(void)
{
   double radian = 0.0;
   int degree = 0;

   printf("Degree\tRadian\n");
   /* A loop to create the contents of the table     */
   /* starts with degree = 0                         */
   /* steps as long as degree is less or equal to 90 */
   /* each step is 10 degrees */
   degree = 0;
   while (degree <= 90)
   {
      radian = (degree * PI) / 180;
      printf("%4d\t%5.2lf\n", degree, radian);

      degree = degree + 10;
   }

   return 0;
}
