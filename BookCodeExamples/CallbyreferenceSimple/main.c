#include <stdio.h>
#include <string.h>

typedef struct {
   char name[36];
   int number;
   double value;
} sensor_t;

// Naming convention: a pointer variable name starts with a p
void changeDouble(double *pdvar);
void changeInt(int *pivar);

int main(void)
{
   double dvalue = 1.123;
   printf("Call by reference using reference to variable '&':\n");
   printf("dvalue before changeDouble(&dvalue): %lf\n", dvalue);
   changeDouble(&dvalue);
   printf("dvalue after  changeDouble(&dvalue): %lf\n", dvalue);
   printf("\n");

   int ivalue = 5;
   printf("Call by reference using reference to variable '&':\n");
   printf("ivalue before changeInt(&ivalue): %d\n", ivalue);
   changeInt(&ivalue);
   printf("ivalue after  changeInt(&ivalue): %d\n", ivalue);
   printf("\n");

   return 0;
}

void changeDouble(double *pdvar)
{
   *pdvar = 23.6; // use dereferencing '*' to change value pdvar points to
}

void changeInt(int *pivar)
{
   *pivar = 15; // use dereferencing '*' to change value pivar points to
}
