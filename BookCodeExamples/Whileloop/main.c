#include <stdio.h>

/* Demonstrates a simple while statement */

int main(void)
{
   int motorspeed = 0;

   while (motorspeed < 15)
   {
      printf("Speeding up motor, current speed is: %d\n", motorspeed);
      motorspeed = motorspeed + 4; // simulate increasing speed
      printf("Speeding up motor, new speed is: %d\n", motorspeed);
   }
   printf("Final motor speed is: %d\n", motorspeed);

   return 0;
}
