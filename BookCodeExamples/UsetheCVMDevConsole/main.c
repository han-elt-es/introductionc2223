#include "devConsole.h"
#include "display.h"
#include "keyboard.h"

/// Example using the display and keyboard modules from CVM-1 v1.8
/// The following files are needed to use the CVM interface:
/// appinfo.h, display.h, keyboard.h, systemErrors.h, devConsole.h
/// display.c, keyboard.c, systemErrors.c, devConsole.c
/// appinfo.h must be customized, fill in programname and version
/// display.h and keyboard.h must be included

int main(void)
{
   // DCS will initialise DSP and KYB
   DCSinitialise();

   // Show welcome message on row 3 and ask for some input
   DSPshow(3, "WELCOME");
   DSPshow(5, "-- Please enter an integer value --");

   DCSsimulationSystemInfo("Integer value = ");

   int inp = KYBgetint(-1);
   if (inp != -1)
   {
      DSPshow(6, "-- The entered integer is %d.", inp);
   }
   else
   {
      DSPshow(6, "-- Unknown integer format!", inp);
   }

   // Clear display starting at row 3
   DSPshowDelete(3, "GOODBYE");

   return 0;
}
