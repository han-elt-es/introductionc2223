TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        devConsole.c \
        display.c \
        keyboard.c \
        main.c \
        systemErrors.c

HEADERS += \
   devConsole.h \
   appInfo.h \
   display.h \
   keyboard.h \
   systemErrors.h

DISTFILES += \
   out.txt \
   out1.txt \
   out2.txt \
   out3.txt \
   out4.txt
