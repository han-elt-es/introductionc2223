#include "OhmsLaw.h"

/* Complete declaration and definition of the calculation functions: */

double calcVoltage(double I, double R)
{
   return I * R;
}

double calcCurrent(double U, double R)
{
   return U / R;
}

double calcResistance(double U, double I)
{
   return U / I;
}
