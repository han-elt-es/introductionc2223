/* Ohm's law calculations modular implementation */

/* Include the prototypes of the modules functions */
#include "OhmsLaw.h"
#include "PrintFunctions.h"

#include <stdio.h>

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U1 = 0.0;
   double I1 = 5.2;
   double R1 = 32.0;

   /* Some function calls with different parameters */
   U1 = calcVoltage(I1, R1);
   printAll(I1, "I1", R1, "R1", U1, "U1");

   double U2 = 24.0;
   double R2 = 5.0;
   double I2 = calcCurrent(U2, R2);
   printAllSingleLine(I2, "I2", R2, "R2", U2, "U2");

   U2 = 5.0;
   I2 = 0.001;
   R2 = calcResistance(U2, I2);
   printAll(I2, "I2", R2, "R2", U2, "U2");

   return 0;
}
