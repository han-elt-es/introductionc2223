/* Prints pseudo-random numbers */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N_RND 5

int main()
{
   int rnd = 0;
   time_t t;
   int now = 0;

   printf(
      "INFO: rand() returns value between 0 and %d\n"
      "             0 and %d included\n",
      RAND_MAX, RAND_MAX);

   // standard srand(1)
   printf("\n-- seed = 1\n");
   for (int count = 0; count < N_RND; count++)
   {
      /* value between 0 and RAND_MAX */
      rnd = rand();
      printf("rnd = %d\n", rnd);
   }

   printf("\n-- seed = 123\n");
   srand(123);
   for (int count = 0; count < N_RND; count++)
   {
      rnd = rand();
      printf("rnd = %d\n", rnd);
   }

   printf("\n-- seed = 123\n");
   srand(123);
   for (int count = 0; count < N_RND; count++)
   {
      rnd = rand();
      printf("rnd = %d\n", rnd);
   }

   now = time(&t);
   printf("\n-- seed = %d (time now)\n", now);
   srand(now);
   for (int count = 0; count < N_RND; count++)
   {
      rnd = rand();
      printf("rnd = %d\n", rnd);
   }

   return 0;
}
