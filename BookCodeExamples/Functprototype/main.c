/* Ohm's law calculations */

#include <stdio.h>

/* Prototype of the Function calcVoltage: */
double calcVoltage(double I, double R);

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U = 0.0;
   double I = 5.2;
   double R = 32.0;

   /* some function calls with different parameters */

   U = calcVoltage(I,R);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(5.0,R);
   printf("I = %.1lf A\n", 5.0);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(I,3.2);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", 3.2);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(5.3,1.2);
   printf("I = %.1lf A\n", 5.3);
   printf("R = %.1lf Ohm\n", 1.2);
   printf("U = %.1lf V\n\n", U);

   return 0;
}

/* Complete declarartion and definition of the Function calcVoltage:
 *    calculates and returns the voltage
 *    the function takes two parameters (values) current I and resitance R */

double calcVoltage(double I, double R)
{
  return I * R;
}
