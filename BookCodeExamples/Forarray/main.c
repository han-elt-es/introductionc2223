#include <stdio.h>

/* typical use of the for loop */
#define NUMBER_OF_DESERTS 4

typedef struct
{
   char name[10];
   double price;
} desert_t;

int main(void)
{
   desert_t deserts[NUMBER_OF_DESERTS] = {{"Pudding", 1.0},
                                          {"Tiramisu", 1.5},
                                          {"Icecream", 0.9},
                                          {"Coffee", 1.2}
                                         };
   printf("Desert pricelist\n");
   printf("Price\t\tDesert \n");

   /* looping through the array to show all the deserts */

   for (int i = 0; i < NUMBER_OF_DESERTS; i++)
   {
      printf("%4.2f euro\t%s\n", deserts[i].price, deserts[i].name);
   }
   return 0;
}
