#ifndef PUTSTRING_H
#define PUTSTRING_H

/* Put a string on the screen using putchar */
int putstring(char text[]);

#endif
