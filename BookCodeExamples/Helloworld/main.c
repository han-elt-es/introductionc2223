/* Prints messages on the screen */

#include <stdio.h>

int main(void)
{
   printf("Hello World\n");
   printf("Hello C programmer\n");

   return 0;
}
