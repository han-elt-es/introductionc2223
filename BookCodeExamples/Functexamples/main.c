/* Ohm's law calculations */

#include <stdio.h>

/* Function calcVoltage:
 *    calculates and returns the voltage
 *    the function takes two parameters (values) current I and resitance R */

double calcVoltage(double I, double R)
{
  return   I * R;
}

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U = 0.0;
   double I = 5.2;
   double R = 32.0;
   double P = 0.0;



   /* Print all values: U, I and R */
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);

   /* Use the function  calcVoltage as part of another statement */
   printf("U = %.1lf V\n\n", calcVoltage(I,R));

   /* Use the function calcVoltage as part of another expression */
   P = calcVoltage(I,R) * I;
   printf("P = %.1lf W\n\n", P);

   /* Use the function calcVoltage as part of a decision */
   if (calcVoltage(I,R) > 10)
   {
      printf("Result is more than 10 V\n\n");
   }
   else
   {
      printf("Result is less or equal to 10 V\n\n");
   }

   /* Use the function calcVoltage as part of a loop and a expression */
   I = 1.0;
   R = 10.0;
   while (calcVoltage(I,R) < 24)
   {
      printf("Current voltage is %.1lf V\n", calcVoltage(I,R));
      R = R + 2;
   }
   printf("Resulting voltage is %.1lf V\n", calcVoltage(I,R));

   /* some function calls with different parameters */

   U = calcVoltage(I,R);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(5.0,R);
   printf("I = %.1lf A\n", 5.0);
   printf("R = %.1lf Ohm\n", R);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(I,3.2);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n", 3.2);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(5.3,1.2);
   printf("I = %.1lf A\n", 5.3);
   printf("R = %.1lf Ohm\n", 1.2);
   printf("U = %.1lf V\n\n", U);

   U = calcVoltage(5,1);
   printf("I = %.1lf A\n", 5);
   printf("R = %.1lf Ohm\n", 1);
   printf("U = %.1lf V\n\n", U);

   return 0;
}
