/* Ohm's law calculations */

#include <stdio.h>

int main(void)
{
   /* Defining declarations, double data type and initialisations */
   double U = 0.0;
   double I = 5.2;
   double R = 32.0;

   /* Calculate U by an arithmetic expression */
   U = I * R;

   /* Print all values: U, I and R */
   printf("U = %.1lf V\n", U);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   I = 15.2;
   R = 32.0;
   /* Calculate U by an arithmetic expression */
   U = I * R;

   /* Print all values: U, I and R */
   printf("U = %.1lf V\n", U);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   I = 20.4;
   R = 32.0;
   /* Calculate U by an arithmetic expression */
   U = I * R;

   /* Print all values: U, I and R */
   printf("U = %.1lf V\n", U);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   return 0;
}
