#include <stdio.h>

/* Simple for loop */
int main(void)
{
   int a = 0;

   for (int i = 10; i < 5; i++)
   {
      a = a + 5;
      printf("i = %d, a = %d\n", i, a);
   }
   
   return 0;
}
