#include <stdio.h>

/* Multiple if statements to determine the state of a motor toggle */

int main(void)
{
   int motorToggle = 3; // value of the motor toggle

   if (motorToggle == 1)
   {
      printf("motorToggle equals 1: switch motor on\n");
   }
   if (motorToggle == 2)
   {
      printf("motorToggle equals 2: motor to half power\n");
   }
   if (motorToggle == 3)
   {
      printf("motorToggle equals 3: motor to full power\n");
   }
   if (motorToggle == 4)
   {
      printf("motorToggle equals 4: switch motor brake on\n");
   }
   if (motorToggle == 5)
   {
      printf("motorToggle equals 5: switch motor off\n");
   }

   return 0;
}
