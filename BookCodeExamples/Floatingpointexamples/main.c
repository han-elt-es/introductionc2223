#include <stdio.h>

int main(void)
{
   float a = 4.0;
   double b = 3.12345678901234567890;

   printf("Value of a: %f\n",a);
   printf("Value of b: %.20lf\n",b);

   /* some calculations with floating point numbers */

   a = 7.0 / 3.14;      // division
   b = 6.8177261 * 3;   // multiplication

   printf("Value of 7.0 / 3.14: %f\n",a);
   printf("Value of 6.8177261 * 3: %.8lf\n",b);

   /* remember the integer calculations */
   printf("\nHave a good look at these results:\n");
   b = 25 / 9;
   printf("Value of 25 / 9: %.2lf\n",b);
   b = 25 / 9.0;
   printf("Value of 25 / 9.0: %.2lf\n",b);
   b = 25.0 / 9;
   printf("Value of 25.0 / 9: %.2lf\n",b);
   b = 25.0 / 9.0;
   printf("Value of 25.0 / 9.0: %.2lf\n",b);

   return 0;
}
