#include <stdio.h>

/* Show the conversion of Celsius to Fahrenheit in a table */

int main(void)
{
   int celsius = 0;
   double fahrenheit = 0.0;
   double celsius_rev = 0.0;

   printf("T in C\tT in F\tT to C reverted\n");

   /* The right code in a simple loop */
   celsius = 0;               // is starts with celsius is 0
   // loop as longs as celsius is less or equal to 30
   while (celsius <= 30)
   {
      //calculate Celsius to Fahrenheit
      fahrenheit = (celsius * 1.8 ) + 32;
      //calculate Fahrenheit to Celsius
      celsius_rev = (fahrenheit - 32) / 1.8;
      //print the original value, converted and reverted
      printf("%4d\t%5.2lf\t%5.2lf\n", celsius, fahrenheit, celsius_rev);
      //increase the celsius value for the next calculation
      celsius = celsius + 5;
   }
   return 0;
}
