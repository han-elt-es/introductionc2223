#include <stdio.h>

/* Shows degrees to radians from 0 - 90          */
/* Using the formula: radian = (degree x PI)/180 */

/* The constant PI is specified as 3.14 */
#define PI 3.14

int main(  void )
{
   int degree = 0; double radian = 0.0;

                    /* Create the table, use \t to show tab's */
   printf("Degree\tRadian\n"); degree = 0; radian = (degree * PI) / 180;
     printf("%d\t%.2lf\n", degree, radian);
 degree = 10;
radian = (degree * PI) / 180;
printf("%d\t%.2lf\n", degree, radian);
 degree = 20;
           radian = (degree * PI) / 180;
printf("%d\t%.2lf\n", degree, radian);
degree = 30;

radian = (degree * PI) / 180;
/* print degree and radian */
                printf("%d\t%.2lf\n", degree, radian);return 0;
  }
