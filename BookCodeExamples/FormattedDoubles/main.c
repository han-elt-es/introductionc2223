#include <stdio.h>

/* Formatted output of doubles, readable output format */
int main(void)
{
   double d1 = 1.1;
   double d2 = -22.22;

   /* default precision 6 */
   printf("standard format: '%lf' '%lf' \n\n", d1, d2);

   printf("9 positions    : '%9lf' '%9lf' \n\n", d1, d2);

   printf("10 pos 2 digits: '%10.2lf' '%10.2lf' \n\n", d1, d2);

   printf("left aligned   : '%-10.2lf' '%-10.2lf' \n\n", d1, d2);

   printf("+ sign         : '%+10.2lf' '%+10.2lf' \n\n", d1, d2);

   printf("+ leading 0    : '%+010.2lf' '%+010.2lf' \n\n", d1, d2);

   printf("pos <          : '%1.3lf' '%1.3lf' \n\n", d1, d2);

   printf("exponential    : '%12.0e' '%12.1e' \n\n", d1, d2);

   return 0;
}
