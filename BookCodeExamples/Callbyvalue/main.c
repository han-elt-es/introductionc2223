#include <stdio.h>

int returnSum(int a, int b)
{
   return a + b;
}

int returnChangedParameter(int result, int a, int b)
{
   result = a + b;
   return 0;
}

int returnChangedParameter2(int *result, int a, int b)
{
   *result = a + b;
   return 0;
}

int main(void)
{
   int c = 0;
   int q = 5;
   int r = 7;
   int result = 0;

   printf("Three function to add two integers:\n");

   result = returnSum(q, r);
   printf("Result returnSum(q, r): q = %d, r = %d, result = %d)\n", q, r, result);

   result = 0;
   c = returnChangedParameter(result, q, r);
   printf("Result returnChangedParameter(result, q, r): q = %d, r = %d, result = %d)\n", q, r, result);

   result = 0;
   c = returnChangedParameter2(&result, q, r);
   printf("Result returnChangedParameter2(&result, q, r): q = %d, r = %d, result = %d)\n", q, r, result);

   return 0;
}
